# About Dahabian.org

This site's made by dahabians for dahabians, we are here to explore together our **beloved Dahab**. 

We welcome posts that are:

* detailed and specific, 
* written clearly


But we recommend that you make use of the search services to see if your question has already been asked (perhaps even answered!) before you ask. But if you end up asking a question that has been asked before, that is fine too. Other users will hopefully edit in links to related or similar questions to help future visitors find their way.

## Contact

To contact the site managers please email hello@dahabian.org
## Licensing

Creative Commons License

All content on Dahabian.org is licensed via the [Creative Commons Attribution 4.0 International License][license].

![](http://i.creativecommons.org/l/by/4.0/88x31.png)

[license]: http://creativecommons.org/licenses/by/4.0/

This license requires that you attribute the information you find here either to the author and/or to the site, depending on the scope and presentation of the information.

Our community supports the [fair use policy][fair-use] when it comes to content created by users of this site.

[fair-use]: http://en.wikipedia.org/wiki/Fair_use

## Bug reporting

Report bugs or feature requests in the [issue tracker][issues].

[issues]: https://gitlab.com/dahabian/dahabian/-/issues

## Copyright

Copyright by [Dahabian.org][copyright].

[copyright]: https://dahabian.org