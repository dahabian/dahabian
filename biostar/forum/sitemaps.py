from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from biostar.forum.models import Post


class StaticSitemap(Sitemap):
    """Reverse 'static' views for XML sitemap."""
    changefreq = "hourly"
    priority = 0.5

    def items(self):
        # Return list of url names for views to include in sitemap
        return ['tags_list', 'community_list', 'post_list']

    def location(self, item):
        return reverse(item)


class DynamicSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Post.objects.all()
